﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    class Motorcycle : Vehicle
    {
        public string MotorcycleInfo
        {
            get
            {
                return $"Motorcycle Id :{Id} Motorcycle Brand :{brand} Motorcycle Colour:{colour} Motorcycle Milage:{milage}";
            }
        }
    }
}
