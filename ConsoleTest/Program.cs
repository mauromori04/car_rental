﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleTest
{
    class Program
    {
        public static string appPath = Directory.GetCurrentDirectory();
        static void Main(string[] args)
        {
            Console.WriteLine("GetFolderPath: {0}", appPath);
            bool run = true;
            while (run)
            {
                //input control
                bool valid = true;

                int answer = -1;

                while (valid)
                {
                    //menu
                    Console.WriteLine("###################################");
                    Console.WriteLine("1. add a vehicle");
                    Console.WriteLine("2. show all vehicles");
                    Console.WriteLine("3. search for a vehicle");
                    Console.WriteLine("4. Delete a vehicle");
                    Console.WriteLine("5. Update a vehicle ");
                    Console.WriteLine("6. Quit ");
                    Console.WriteLine("###################################");
                    valid = int.TryParse(Console.ReadLine(), out answer);
                    if ((answer < 1 || answer > 7) && valid == false)
                    {
                        valid = false;
                        Console.WriteLine("Please enter a number between 1 and 6");
                    }

                    //declaring the car obj and the cars list for later use
                    Vehicle vehicle = new Vehicle();

                    var vehicles = new List<Vehicle>();

                    switch (answer)
                    {
                        case 1:
                            {
                                Console.WriteLine("Enter vehicle Id");
                                vehicle.Id = int.Parse(Console.ReadLine());
                                bool i = true;
                                while (i)
                                {
                                    Console.WriteLine("Enter Vehicle Type (car, truck, motorcycle");
                                    vehicle.type = Console.ReadLine();
                                    switch (vehicle.type)
                                    {
                                        case "car":
                                            {
                                                i = false;
                                                break;
                                            }
                                        case "truck":
                                            {
                                                i = false;
                                                break;
                                            }
                                        case "motorcycle":
                                            
                                                i = false;
                                                break;
                                            }
                                    }
                                


                                Console.WriteLine("Enter vehicle Brand");
                                vehicle.brand = Console.ReadLine();

                                Console.WriteLine("Enter vehicle Colour");
                                vehicle.colour = Console.ReadLine();

                                Console.WriteLine("Enter vehicle Milage");
                                vehicle.milage = int.Parse(Console.ReadLine());

                                vehicles = CarHelper.RetrieveCars();

                                vehicles.Add(vehicle);

                                CarHelper.SendListToFile(vehicles);

                                vehicles.Clear();

                                break;
                            }
                        case 2:
                            {
                                vehicles = CarHelper.RetrieveCars();

                                foreach (var item in vehicles)
                                {
                                    Console.WriteLine(item.VehicleInfo);
                                }

                                Console.WriteLine("\n");

                                vehicles.Clear();

                                break;
                            }
                        case 3:
                            {
                                vehicles = CarHelper.RetrieveCars();

                                Console.WriteLine("type vehicle id:");

                                int id = int.Parse(Console.ReadLine());

                                bool x = false;

                                foreach (var item in vehicles)
                                {
                                    if (item.Id == id)
                                    {
                                        x = true;
                                        Console.WriteLine("found vehicle");

                                        Console.WriteLine(item.VehicleInfo); ;
                                    }

                                }
                                if (!x)
                                {
                                    Console.WriteLine("vehicle not found Try Again");
                                }
                                Console.WriteLine("\n");

                                vehicles.Clear();

                                break;
                            }
                        case 4:
                            {
                                vehicles = CarHelper.RetrieveCars();

                                Console.WriteLine("type vehicle id:");

                                int id = int.Parse(Console.ReadLine());

                                Vehicle item = vehicles.Find(x => x.Id == id);

                                if (item != null)
                                {
                                    vehicles.Remove(item);

                                    Console.WriteLine("vehicle with id:" + id + " has been removed");
                                }
                                else
                                {
                                    Console.WriteLine("vehicle Not Found Try Again");
                                }
                                CarHelper.SendListToFile(vehicles);

                                Console.WriteLine("\n");

                                vehicles.Clear();

                                break;
                            }
                        case 5:
                            {
                                vehicles = CarHelper.RetrieveCars();

                                Console.WriteLine("search by id:");

                                int id = int.Parse(Console.ReadLine());

                                int index = -1;

                                foreach (var item in vehicles)
                                {
                                    if (item.Id == id)
                                    {
                                        index = vehicles.IndexOf(item);
                                    }
                                }
                                if (index != -1)
                                {

                                    vehicle.Id = id;

                                    Console.WriteLine("Type new Brand");
                                    vehicle.brand = Console.ReadLine();

                                    Console.WriteLine("Type new Colour");
                                    vehicle.colour = Console.ReadLine();

                                    Console.WriteLine("Type new Milage");
                                    vehicle.milage = int.Parse(Console.ReadLine());

                                    vehicles.Insert(index, vehicle);

                                    vehicles.RemoveAt(index + 1);

                                    CarHelper.SendListToFile(vehicles);
                                }
                                else
                                {
                                    Console.WriteLine("vehicle Not Found");
                                }

                                vehicles.Clear();

                                break;
                            }
                        case 6:
                            {
                                Environment.Exit(-1);
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Something went wrong");

                                break;
                            }

                    }
                }
            }
        }
    }
}
