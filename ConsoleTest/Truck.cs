﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    class Truck : Vehicle
    {
        public string TruckInfo
        {
            get
            {
                return $"Truck Id :{Id} Truck Brand :{brand} Truck Colour:{colour} Truck Milage:{milage}";
            }
        }
    }
}
