﻿
namespace ConsoleTest
{
    class Vehicle
    {
        public int Id { get; set; }
        public string type { get; set; }
        public string brand { get; set; }
        public string colour { get; set; }
        public int milage { get; set; }

        public string VehicleInfo
        {
            get
            {
                return $"Vehicle Id :{Id} Vehicle Type :{type} Vehicle Brand :{brand} Vehicle Colour:{colour} Vehicle Milage:{milage}";
            }
        }

    }
}
