﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    class Car : Vehicle
    {
        public string CarInfo
        {
            get
            {
                return $"Car Id :{Id} Car Brand :{brand} Car Colour:{colour} Car Milage:{milage}";
            }
        }
    }
}
